#Build Manager class
Class BuildManager {
#properties
#private properties	
	hidden [object]$app;
	hidden [object]$nugetSources;
	hidden [string]$publishProfileXml;
	hidden [string]$commandFormat = ". '{0}\Scripts\Deploy-FabricApplication.ps1' -ApplicationPackagePath '{0}\pkg\Debug' -PublishProfileFile '{0}\PublishProfiles\{1}' -DeployOnly:`$false -ApplicationParameter:`$applicationParameter -UnregisterUnusedApplicationVersionsAfterUpgrade `$false -OverrideUpgradeBehavior 'None' -OverwriteBehavior 'SameAppTypeAndVersion' -SkipPackageValidation:`$false -ErrorAction Stop"
	
	hidden static [BuildManager]$instance;

#static functions
	#Singleton function 
	static [BuildManager]GetInstance() {
		if ([BuildManager]::instance -eq $null) { 
			[BuildManager]::instance = [BuildManager]::new()
		}
        
		return [BuildManager]::instance;
	}

#public access function 	
	[void] Initialize(
		[string] $publishProfileXml, 
		[string] $nugetSourceConfigXml = "NugetSourceConfig.xml")
	{
		$this.publishProfileXml = $publishProfileXml;
		$this.nugetSources = $this.LoadXmlNodes($nugetSourceConfigXml);
	}
	
	[void] Build([object]$app){
		$this.app = $app;
		$this.RestoreProject();
		$this.BuildProject();
	}
	
	#Publish To fabric
	[string] Publish() {
		if($this.app.web -eq "true"){
		 	$ngCommand = "ng serve -c {0}" -f $this.app.applicationEnvironment
			
			return $ngCommand
		} else {
			$serviceCommand = $this.commandFormat -f $this.app.SfProjDir, $this.publishProfileXml
			
			return $serviceCommand
		}
	}
#can be access if use -Force flag 
	#private (hidden) function
	#find the current vs2017 or vs2015 msBuild.exe
	hidden [string] ResolveMsBuild() {
		$msb2017 = Resolve-Path "${env:ProgramFiles(x86)}\Microsoft Visual Studio\*\*\MSBuild\*\bin\msbuild.exe" -ErrorAction SilentlyContinue
		if($msb2017 -and $msb2017.length -gt 0) {
			Write-Host "Found MSBuild 2017 (or later)."
			Write-Host $msb2017

			return $msb2017[0].path
		}

		$msBuild2015 = "${env:ProgramFiles(x86)}\MSBuild\14.0\bin\msbuild.exe"

		if(-not (Test-Path $msBuild2015)) {
			throw 'Could not find MSBuild 2015 or later.'
		}

		Write-Host "Found MSBuild 2015."
		Write-Host $msBuild2015

		return $msBuild2015
	}
	
	hidden [string] GetMsBuilder(){
    	if (-not (Test-Path env:msbuild)) {
			return $this.ResolveMsBuild()
		}
		else {
            return $env:msbuild  
		}
	}
	
	hidden [object] LoadXmlNodes([string] $xmlFilePath){
    	return (select-xml -xpath /nugetsources/nugetsource -path $xmlFilePath).node
	}
	
	#Restore Packages
	hidden [void] RestoreProject() {
		if($this.app.web -eq "true") {
			npm install -g
		} else {
            $nuget = "{0}\{1}" -f $PSScriptRoot, "nuget.exe" 
			
			#As configured in your "Package sources" settings in VS
			foreach($nugetSource in  $this.nugetSources) 
			{
				&$nuget sources update -name $nugetSource.Name -username $nugetSource.username -password $nugetSource.password
			}
			
            &$nuget restore -NoCache >> log.txt
			&$nuget restore -NoCache 
        }
	}
	
	#build Project
	hidden [void] BuildProject() {
		if($this.app.web -eq "true") {
			ng build -c $this.app.applicationEnvironment
		} else {
			write-host "Building " $this.app.SlnPath
			$msBuild = $this.GetMsBuilder();
            		    
			#execute msBuild 
			&$msBuild -t:restore $this.app.SlnPath >> log.txt
            
			Write-host "Build Finished"
			
			#package App
			Write-host "Packaging " $this.app.SfprojFilePath
			&$msBuild $this.app.SfprojFilePath /t:Package /p:Configuration=Debug
		}
	}	
} 