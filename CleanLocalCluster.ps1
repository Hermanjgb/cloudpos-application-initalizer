Param([string]$serviceFabricHost = "localhost:19000")

Connect-ServiceFabricCluster $serviceFabricHost

$sfApps = Get-ServiceFabricApplication
$sfAppTypes = Get-ServiceFabricApplicationType

foreach($sfApp in  $sfApps) 
{
	write-host ("Deleting Application {0}" -f $sfApp.ApplicationName)
	Remove-ServiceFabricApplication -ApplicationName $sfApp.ApplicationName.AbsoluteUri -Force
 }
 
 foreach($sfAppType in  $sfAppTypes) 
{
	write-host ("Unprovisioning Type {0}" -f $sfAppType.ApplicationTypeName)
	Unregister-ServiceFabricApplicationType -ApplicationTypeName $sfAppType.ApplicationTypeName -ApplicationTypeVersion 1.0.0 -Force
 }