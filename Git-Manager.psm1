#git manager class
Class GitManager {
#properties
#private properties	
	hidden [Object]$git;
	hidden [Object]$app;
	hidden [hashtable] $Callbacks;
	hidden [string]$workingBranch;
	hidden [bool]$stashed;
	hidden static [GitManager]$instance;
	
#constructor	
	GitManager(){
		$pullMain = { $this.MoveToMainBranch };
        $updateCurrent = {$this.UpdateCurrentBranch };
		
		$this.Callbacks = @{
			"pull-main" = $pullMain;
			"update-current" = $updateCurrent
		}
	}
	
#static functions
	#Singleton function 
	static [GitManager]GetInstance() {
		if ([GitManager]::instance -eq $null) { 
			[GitManager]::instance = [GitManager]::new()
		}
        
		return [GitManager]::instance;
	}
	
#public access function 
	#set the repo values return the current updated object
	[GitManager]SetupCurrentRepo([object]$app){
		#set current repo
		$this.git = $app.git;
		$this.app = $app;
		$this.workingBranch = [string]::Empty;
		$this.stashed = $false;
		
		return $this;
	}
	
	[void] GitRequiredActions(){
		cd ..
		
		if(-not (Test-Path -Path $this.app.path)) {		
			$this.PullNewRepository()
		}else {
			cd $this.app.path
		}
				
		if($this.app.web -eq "true"){
			cd $this.app.webFolder
		}
		
		$this.workingBranch = $this.GetCurrentBranch()
		
		Write-host $this.workingBranch
		
		foreach($action in $this.git.Actions){
			$this.Callbacks[$action].Invoke();
		}
	}
	
	[void]GitCompleteActions(){
		if($this.git.goBackToWorkingDir -and $this.workingBranch -NotLike $this.GetCurrentBranch){
			$this.MoveToWorkingBranch
		}
		
		cd ..
		
		if($this.app.web -eq "true"){
			cd ..
		}
		
		cd $PSScriptRoot
	}
	
#can be access if use -Force flag 
	#private (hidden) function

	#First time execution 
	#pull the requested repository
	hidden [void]PullNewRepository() {
		git clone $this.git.url
		cd $this.app.path
		git checkout  $this.git.branch
		git pull
		
		if ($this.git.containssubmodule) 
		{
			git submodule update --remote
		}
		
		Write-host "Repo cloned, and current branch $($this.git.branch)" 
	}

	#First 
	hidden [void]MoveToMainBranch() {
		if($this.workingBranch -NotLike $this.git.branch){
			$this.MoveToBranch($this.git.branch)
		}
		
		git pull
	}
	
	#Last
	hidden [void]MoveToWorkingBranch(){
		git checkout $this.workingBranch
	}
	
	
	hidden [void]UpdateCurrentBranch() {
		$this.MoveToMainBranch();
		$this.MoveToBranch($this.workingBranch);
		
		git merge $this.git.branch
		
		if($this.stashed){
			git stash pop
		}
	}
	
#Helper actions
	#move repo to target branch
	hidden [void]MoveToBranch([string]$targetBranch){
		if($this.RepoHasBeenModified)
		{
			Write-host "No changes"
			git checkout $targetBranch
		}
		else
		{
			Write-host "There are some changes"
			git stash --include-untracked
			$this.stashed = $true;
			git checkout  $targetBranch
		}
	}
	
	#return the current local branch 
	hidden [string]GetCurrentBranch() {
		return git rev-parse --abbrev-ref HEAD
	}
	
	#check with porcelain if there has been any changes in the current local branch
	hidden [bool] RepoHasBeenModified(){
		$gitStatus = git status --porcelain
	
		return $gitStatus.Length -eq 0
	}	
}