# Cloudpos application initializer

Fabric service manager

## Required dependencies

1. Visual Studio 2017 or latest*
2. [nuget.exe](https://www.nuget.org/downloads) place on the script root folder 
3. [Git Bash](https://git-scm.com/downloads)
4. [Service Fabric SDK](https://docs.microsoft.com/en-us/azure/service-fabric/service-fabric-get-started)
5. [node.js (optional)](https://nodejs.org/en/download/)
6. PowerShell V3.0 or latest*

## Application list XML

### ApplicationList.xml
This XML will contain the list of application plus the required configurations to fetch and deploy into the local Service Fabric Cluster. 

### XML format

```XML
<?xml version="1.0" encoding="UTF-8"?>
<applications>
    <application SlnPath="" path="" SfProjDir="" SfprojFilePath="" web="" >
        <git containssubmodule="">
            <url></url>
            <branch></branch>
        </git>
    </application>    
</applications>
```

1. applications: Main tag and host for the list of applications to be managed by the script 

2. application: Contains paths for the application so the script can execute the different build process 
    1. SlnPath (string) = path, name and .snl extension for the auto-generated file for a visual studio project 
    2. path (string) = root path hosting the project  
    3. SfProjDir (string) = path for the Service Fabric project   
    4. SfprojFilePath (string) = path for the .sfproj file 
    5. web (true/false) =  currently only for angular applications 

3. git: this sub-tag contains the information required to clone and pull the latest updates from a git repository
    1. containssubmodule (true/false) = if the project is using git sub-repositories then this flag will take care of pulling those changes
    2. url = ssh URL path for the git repository
    3. branch = the current working branch 
    
## Restore Nugets

In case of a private artefact repository, the NugetSourceConfig.xml should be filled

```XML
<?xml version="1.0" encoding="UTF-8"?>
<nugetsources>
    <nugetsource name="">
        <username></username>
        <password></password>
    </nugetsource>
</nugetsources>
```

1. nugetsources: nuget sources host tab 
2. nugetsource: source element 
    1. name (string)= name of the source
    2. username = user name credentials
    3. password = password credentials
	
## Running the script

To run the script navigate PowerShell to the **publishCluster.ps1** folder.

```PowerShell
	./publishCluster.ps1
```

this will execute the current default configurations:

```PowerShell
	./publishCluster.ps1 -xmlPath "ApplicationList.xml" -serviceFabricHost "localhost:19000" -publishProfileXml "Local.1Node.xml" -nugetSourceConfigXml "NugetSourceConfig.xml"
```

this variables are:

1. xmlPath = xml file containing the application list format
2. serviceFabricHost = current hosting Service Fabric 
3. publishProfileXml = base on the service fabric current configuration either 1 or 5 nodes configuration file
4. nugetSourceConfigXml = nuget source credetials files 


## Clear service fabric
the **CleanLocalCluster.ps1** will take care of un-register the applications in the currently running service fabric cluster 
without having to restart the whole thing 