#Import modules
using module .\Git-Manager.psm1
using module .\Build-Manager.psm1

#Main context parameters
Param([string]$xmlPath = "ApplicationList.xml", 
	  [string]$serviceFabricHost = "localhost:19000", 
      [string]$publishProfileXml = "Local.1Node.xml",
	  [string]$nugetSourceConfigXml = "NugetSourceConfig.xml")


#___main___ 

if(!(Test-Path "nuget.exe")){
	Write-Host "nuget.exe absent from the script root folder"
	Read-Host "Press any key to exit..."
	exit
}

. Connect-ServiceFabricCluster $serviceFabricHost
$global:clusterConnection = $clusterConnection

$xml = (select-xml -xpath /applications/application -path $xmlPath).node
$selectedApps = $xml | Out-GridView -Title "Selected Applications" -PassThru


[BuildManager]::GetInstance().Initialize($publishProfileXml, $nugetSourceConfigXml);

foreach($app in  $selectedApps) {
	[GitManager]::GetInstance().SetupCurrentRepo($app).GitRequiredActions();
	[BuildManager]::GetInstance().Build($app);
	$serviceCommand = [BuildManager]::GetInstance().Publish();
	iex $serviceCommand
	[GitManager]::GetInstance().GitCompleteActions();
} 

. Remove-Variable ClusterConnection 
